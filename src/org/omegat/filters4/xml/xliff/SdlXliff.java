/**************************************************************************
 OmegaT - Computer Assisted Translation (CAT) tool
          with fuzzy matching, translation memory, keyword search,
          glossaries, and translation leveraging into updated projects.

 Copyright (C) 2017-2020 Thomas Cordonnier
               Home page: http://www.omegat.org/
               Support center: http://groups.yahoo.com/group/OmegaT/

 This file is part of OmegaT.

 OmegaT is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 OmegaT is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.omegat.filters4.xml.xliff;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;

import org.omegat.core.Core;
import org.omegat.core.data.EntryKey;
import org.omegat.core.data.IProject;
import org.omegat.core.data.TMXEntry;
import org.omegat.filters2.Instance;

/**
 * Overrides XLiff-1 for features which only work with SDL-Xliff
 *
 * @author Thomas Cordonnier
 */
public class SdlXliff extends Xliff1Filter {
    
    // ---------------------------- IFilter API ----------------------------
    
    @Override
    public String getFileFormatName() {
        return "SDL XLIFF - StaX";
    }

    @Override
    public Instance[] getDefaultInstances() {
        return new Instance[]  { new Instance("*.sdlxliff") };
    }
    
    @Override
    public boolean isFileSupported(java.io.File inFile, Map<String, String> config, org.omegat.filters2.FilterContext context) {
        try {
            StartElement el = findEvent(inFile, java.util.regex.Pattern.compile(".*/.*:xliff"));
            if (el == null) return false;
			namespace = el.getName().getNamespaceURI();
            if (el.getAttributeByName(new QName("http://sdl.com/FileTypes/SdlXliff/1.0", "version")) != null) return true;                
            return super.isFileSupported(inFile, config, context);
        } catch (Exception npe) {
            return false; // version attribute is mandatory
        }
    }

    // ----------------------------- specific part ----------------------
    
    private String currentMid = null;
    private Map<String, StringBuffer> sdlComments = new TreeMap<>();
    private StringBuffer commentBuf = null;
    private Map<UUID, String> omegatNotes = new TreeMap<>();
    private Map<String, UUID> defaultNoteLocations = new TreeMap<>();
    private Map<EntryKey, UUID> altNoteLocations = new TreeMap<>();
    private Map<String, List<XMLEvent>> tagDefs = new TreeMap<>();
    
    @Override	// also starts on cmt-defs or tag-defs, else like in standard XLIFF
    protected void checkCurrentCursorPosition(javax.xml.stream.XMLStreamReader reader, boolean doWrite) {
		if (reader.getEventType() == StartElement.START_ELEMENT) {
			String name = reader.getLocalName();
			if (name.equals("cmt-defs")) this.isEventMode = true;
			if (name.equals("tag-defs")) this.isEventMode = true;
		}
		super.checkCurrentCursorPosition(reader, doWrite);
	}	
    
    @Override
    protected boolean processStartElement (StartElement startElement, XMLStreamWriter writer) throws  XMLStreamException {
        if (startElement.getName().getLocalPart().equals("cmt-def")) {
            sdlComments.put (startElement.getAttributeByName(new QName("id")).getValue(), commentBuf = new StringBuffer());
            return true;
        }
        if (startElement.getName().getLocalPart().equals("mrk"))
			if (startElement.getAttributeByName(new QName("mtype")).getValue().equals("seg"))
				currentMid = startElement.getAttributeByName(new QName("mid")).getValue();
            else if (startElement.getAttributeByName(new QName("mtype")).getValue().equals("x-sdl-comment")) {
                String id = startElement.getAttributeByName(new QName("http://sdl.com/FileTypes/SdlXliff/1.0", "cid")).getValue();
                this.addNoteFromSource (currentMid, sdlComments.get(id).toString());
            }
        if (startElement.getName().getLocalPart().equals("tag")) 
            tagDefs.put(startElement.getAttributeByName(new QName("id")).getValue(), currentBuffer = new java.util.LinkedList<XMLEvent>());
        if (writer != null)
            if (startElement.getName().equals(new QName("http://sdl.com/FileTypes/SdlXliff/1.0", "seg"))) {
                fromEventToWriter (eFactory.createStartElement(startElement.getName(), null, startElement.getNamespaces()), writer);
                for (java.util.Iterator I = startElement.getAttributes(); I.hasNext(); ) {
                    Attribute A = (Attribute) I.next(); 
                    if (! A.getName().getLocalPart().equals("conf")) 
						writer.writeAttribute(A.getName().getPrefix(), A.getName().getNamespaceURI(), A.getName().getLocalPart(), A.getValue());
					else 
						writer.writeAttribute("conf", "Translated");
                }
                return false; // we already added current element
            }
        return super.processStartElement(startElement, writer);
    } 

    @Override
    protected boolean processEndElement (EndElement endElement, XMLStreamWriter writer) throws  XMLStreamException {
        if (endElement.getName().getLocalPart().equals("tag")) currentBuffer = null;
        if (endElement.getName().getLocalPart().equals("cmt-def")) commentBuf = null;
        if (endElement.getName().getLocalPart().equals("cmt-defs")) {
            this.isEventMode = false;
            if (writer != null) {
                IProject proj = Core.getProject();
                proj.iterateByDefaultTranslations((String source, TMXEntry trans) -> {
                    if (! trans.hasNote()) return;
                    
                    UUID id = UUID.randomUUID(); omegatNotes.put(id, trans.note); defaultNoteLocations.put(source, id); 
                    createSdlNote (id, trans, writer);
                });
                proj.iterateByMultipleTranslations((EntryKey key, TMXEntry trans) -> {
                    if (! trans.hasNote()) return;
                    
                    UUID id = UUID.randomUUID(); omegatNotes.put(id, trans.note); altNoteLocations.put (key, id);
                    createSdlNote (id, trans, writer);
                });
            }
        }
        return super.processEndElement (endElement, writer);
    }
    
    // Do not generate tag for comment inside source
    protected boolean isUntaggedTag (StartElement stEl) {
        return (stEl.getName().equals(new QName("urn:oasis:names:tc:xliff:document:1.2", "mrk"))
            && (stEl.getAttributeByName(new QName("mtype")).getValue().equals("x-sdl-comment") || stEl.getAttributeByName(new QName("mtype")).getValue().equals("x-sdl-added")))
            || super.isUntaggedTag(stEl);
    }    
    
    // Track change 'DELETED' should not appear at all in the 
    protected boolean isDeletedTag (StartElement stEl) {
        return (stEl.getName().equals(new QName("urn:oasis:names:tc:xliff:document:1.2", "mrk"))
            && stEl.getAttributeByName(new QName("mtype")).getValue().equals("x-sdl-deleted"))
            || super.isUntaggedTag(stEl);
    }
    
    @Override
    protected char findPrefix (StartElement stEl) {
        if (stEl.getName().getLocalPart().equals("g")) 
            try {
                String tagId = stEl.getAttributeByName(new QName("id")).getValue();
                List<XMLEvent> contents = tagDefs.get (tagId);
                for (XMLEvent ev: contents)
                    if (ev.isCharacters()) {
                        String txt = ev.asCharacters().getData();
                        if (txt.contains("italic") && ! txt.contains("bold")) return 'i';
                        if (! txt.contains("italic") && (txt.contains("bold") || txt.contains("strong"))) return 'b';
                        if (txt.contains("size")) return 's';
                        if (txt.contains("color")) return 'c';
                        if (txt.contains("footnote")) return 'n';
                        if (txt.contains("cf")) return 'f'; // format
                    } else if (ev.isStartElement()) {
                        String name = ev.asStartElement().getName().getLocalPart();
                        if (name.equals("bpt") || name.equals("ept")) {
                            name = ev.asStartElement().getAttributeByName(new QName("name")).getValue();
                            if (name.equals("italic") || name.equals("em")) return 'i';
                            if (name.equals("bold") || name.equals("strong")) return 'b';
                        }
					}
            }
            catch (Exception e) {}
        // default
        return super.findPrefix (stEl);
	}
	
    private static void createSdlNote(UUID id, TMXEntry trans, XMLStreamWriter writer) {
        try {
            writer.writeStartElement("http://sdl.com/FileTypes/SdlXliff/1.0", "cmt-def"); 
            writer.writeAttribute("id", id.toString());
            writer.writeStartElement("http://sdl.com/FileTypes/SdlXliff/1.0", "Comments"); 
            writer.writeStartElement("http://sdl.com/FileTypes/SdlXliff/1.0", "Comment"); 
            writer.writeCharacters (trans.note);
            writer.writeEndElement(/*Comment*/); writer.writeEndElement(/*Comments*/);  writer.writeEndElement(/*cmt-def*/); 
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected boolean processCharacters (Characters event, XMLStreamWriter writer) { 
        if (commentBuf != null) commentBuf.append(event.toString());
        return super.processCharacters(event, writer);
    }    

    // This method is called only during translation generation: so we can use it to add notes!
    @Override
    protected List<XMLEvent> restoreTags (String unitId, String path, String src, String tra) {
        List<XMLEvent> res = super.restoreTags(unitId, path, src, tra);
        EntryKey key = new EntryKey("", src, unitId, null,null,path); UUID addNote = null;
        if (altNoteLocations.get(key) != null) addNote = altNoteLocations.get(key);        
        else if (defaultNoteLocations.get(src) != null) addNote = defaultNoteLocations.get(src);
        if ((addNote != null) && (omegatNotes.get(addNote) != null)) {
            List<Attribute> attr = new java.util.LinkedList<Attribute>();
            attr.add (eFactory.createAttribute("sdl", "http://sdl.com/FileTypes/SdlXliff/1.0", "cid", addNote.toString()));
            attr.add (eFactory.createAttribute(new QName("mtype"), "x-sdl-comment"));
            res.add (0, eFactory.createStartElement(new QName("urn:oasis:names:tc:xliff:document:1.2", "mrk"), attr.iterator(), null));            
            res.add (eFactory.createEndElement(new QName("urn:oasis:names:tc:xliff:document:1.2", "mrk"), null));             
        }
        return res;
    }

    /** Remove entries with only tags **/
    @Override
    protected boolean isToIgnore (String src, String tra) {
        if (tra == null) return false;
        while (src.startsWith("<")) src = src.substring(Math.max(1, src.indexOf(">") + 1));
        while (tra.startsWith("<")) tra = tra.substring(Math.max(1, tra.indexOf(">") + 1));
        return (src.length() == 0) && (tra.length() == 0);
    }
}