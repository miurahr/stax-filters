/**************************************************************************
 Copyright (C) 2017 DGT-OmegaT (http://185.13.37.79/)
 
 This file is only a stub to enable compilation of Stax Filters plugin.
 
 DGT-OmegaT is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 OmegaT is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.omegat.filters3.xml;

import java.io.*;

public class XMLWriter extends Writer {
	public XMLWriter(File file, String encoding, String eol) throws FileNotFoundException, UnsupportedEncodingException {}
	
    /**
     * Flushes the writer (which does the real write-out of data) and closes the
     * real writer.
     */
    public void close() throws IOException {
    }

    /**
     * Does the real write-out of the data, first adding/replacing encoding
     * statement.
     */
    public void flush() throws IOException {
    }


    /**
     * Write a portion of an array of characters. Simply calls
     * <code>write(char[], int, int)</code> of the internal
     * <code>StringWriter</code>.
     * 
     * @param cbuf
     *            - Array of characters
     * @param off
     *            - Offset from which to start writing characters
     * @param len
     *            - Number of characters to write
     * @throws IOException
     *             - If an I/O error occurs
     */
    public void write(char[] cbuf, int off, int len) throws IOException {
    }
}
