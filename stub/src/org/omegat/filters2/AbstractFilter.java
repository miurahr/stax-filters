/**************************************************************************
 Copyright (C) 2017 DGT-OmegaT (http://185.13.37.79/)
 
 This file is only a stub to enable compilation of Stax Filters plugin.
 
 DGT-OmegaT is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 OmegaT is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.omegat.filters2;

import java.io.*;
import java.util.Map;

/**
 * The base class for all filters
 */
public abstract class AbstractFilter implements IFilter {

    /** Callback for parse. */
    protected IParseCallback entryParseCallback;

    /** Callback for translate. */
    protected ITranslateCallback entryTranslateCallback;

    /** Callback for translate. */
    protected IAlignCallback entryAlignCallback;
	
    protected String inEncodingLastParsedFile;

    public abstract boolean isSourceEncodingVariable();
    public abstract boolean isTargetEncodingVariable();
	public abstract String getFileFormatName();

    public abstract Instance[] getDefaultInstances();
	
    public BufferedReader createReader(File inFile, String inEncoding) throws UnsupportedEncodingException, IOException {
		return null;
	}
	
	public BufferedWriter createWriter(File outFile, String outEncoding) throws UnsupportedEncodingException, IOException {
		return null;
	}
	
    public boolean isFileSupported(File inFile, java.util.Map<String, String> config, FilterContext fc) {
		return false;
	}
	
	public void processFile(File inFile, File outFile, FilterContext fc) throws IOException, TranslationException {
	}
	
    protected void processFile(BufferedReader inReader, BufferedWriter writer, FilterContext fc) throws IOException, TranslationException {
	}
	
	public void setCallbacks(IParseCallback parseCallback, ITranslateCallback translateCallback) {
	}
	
    protected boolean requirePrevNextFields() {
        return false;
    }
	
	public Map<String, String> changeOptions(java.awt.Dialog parent, Map<String, String> currentOptions) {
		return null;
	}
}
