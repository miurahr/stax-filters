/**************************************************************************
 Copyright (C) 2017 DGT-OmegaT (http://185.13.37.79/)
 
 This file is only a stub to enable compilation of Stax Filters plugin.
 
 DGT-OmegaT is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 OmegaT is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.omegat.core.statistics;


/**
 * Class for get/set statistics preferences and settings.
 * 
 * @author Alex Buloichik (alex73mail@gmail.com)
 */
public class StatisticsSettings {

    public static boolean isCountingStandardTags() {
        return false;
    }

}
