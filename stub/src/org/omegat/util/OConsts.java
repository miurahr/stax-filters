/**************************************************************************
 Copyright (C) 2017 DGT-OmegaT (http://185.13.37.79/)
 
 This file is only a stub to enable compilation of Stax Filters plugin.
 
 DGT-OmegaT is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 OmegaT is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.omegat.util;

public class OConsts {


    /**
     * The limit of bytes that AbstractFilter.isFileSupported may read. 8k (8192
     * bytes) for now, as this is the default buffer size for BufferedReader.
     */
    public static final int READ_AHEAD_LIMIT = 65536;


    /** Encoding: "UTF-8". */
    public static final String UTF8 = "UTF-8";
}
